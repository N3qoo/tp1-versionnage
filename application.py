#! /usr/bin/env python3
from db import *
from flask import Flask, render_template
import os
app = Flask(__name__)
app.debug = True

@app.route('/')
def home():
    return render_template("home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):


    bd = DB()
    bd.__init__()
    return render_template("user.html",name=name,todo=bd.get(name))

@app.route('/users/')
def users():
    bd = DB()
    bd.__init__()
    data = []
    for user in bd.users():
        for liste in bd.get(user):
            data.append([liste])
    return render_template("users.html",data = data)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
